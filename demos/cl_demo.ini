[runtime]
sampler = test

[test]
save_dir=cl_outdir
fatal_errors=T

[output]
filename = cl_output.txt
format = text
verbosity= debug

[pipeline]
modules = consistency camb rescale halofit_camb owls smail_shear smail_LSS IA growth clerkin luminosity apply_biases spectra plot_spectra
values = demos/values_cl_demo.ini

likelihoods=

extra_output = 
quiet=F
debug=T
timing=T

[consistency]
file = cosmosis-standard-library/utility/consistency/consistency_interface.py

[camb]
file = cosmosis-standard-library/boltzmann/camb/camb.so
mode=all
lmax=2500
feedback=1
k_spacing_boost=1.0
get_transfer=T

[rescale]
file=cosmosis-standard-library/utility/sample_sigma8/sigma8_rescale.py

[halofit_camb]
file= cosmosis-standard-library/boltzmann/halofit_takahashi/halofit_interface.so
kmin=1e-3
kmax=91.0
nk=100

; DES library module that adds baryonic effects. Not essential to the pipeline
[owls]
file=cosmosis-des-library/wl/owls/owls_interface.py
mode=fixed
powtable=${COSMOSIS_SRC_DIR}/cosmosis-des-library/wl/owls/powtable_AGN_all.dat

[IA]
file=cosmosis-standard-library/intrinsic_alignments/la_model/linear_alignments_interface.py
method='krhb'
bias=T

[growth]
file=/home/sws/cosmosis/modules/modules/growth/interface.so

[clerkin]
file=cosmosis-standard-library/bias/clerkin/clerkin_interface.py
model='gtd'
mode='bias'

; Call the n(z) module once for each galaxy sample
; Rather than a general wl_number_density section it now saves the n(z) to
; the survey section of the datablock
[smail_shear]
file=cosmosis-standard-library/number_density/smail/photometric_smail.py
survey=des
dz=0.005
; Catastrophic outlier parameters
; Set the first to 'None' or omit altogether to turn catastrophic outliers off
; Use 'uniform' for a flat distribution and 'island' for a Gaussian island of outliers
catastrophic_outliers='uniform'
fcat = 0.05			; fraction within selected window that are outliers
dzcat = 0.129			; width of the window from which the outliers are scattered
zcat0= 0.65			; centre point of that region
zcat = 0.5			; centre of outlier island
sigcat= 0.1			; width of outlier island

[smail_LSS]
file=cosmosis-standard-library/number_density/smail/photometric_smail.py
survey=des_lrg
dz=0.005
catastrophic_outliers='uniform'
fcat = 0.05
dzcat = 0.129
zcat0= 0.65
zcat = 0.5
sigcat= 0.1

; Calculates the slope of the luminosity function using a COMBO-17 fitting function
; Note that the survey chosen here must match the LSS catalogue chosen for spectra below
[luminosity]
file=cosmosis-standard-library/luminosity_function/Joachimi_Bridle_alpha/interface.py
survey=des_lrg
binned_alpha=T

; Applies relevant combinations of galaxy and IA bias to calculate the 3D 
; power spectra from the nonlinear matter power spectrum
[apply_biases]
file=cosmosis-standard-library/shear/apply_astrophysical_biases/interface.so
intrinsic_alignments=T
galaxy_bias=T
verbosity=2

; The main calculation- this reads the n(z) and p(k,z) and evaluates the Limber
; integral in each redshift bin pairing
[spectra]
file=cosmosis-standard-library/shear/spectra/interface.so
LSS_survey=des_lrg	 
shear_survey=des	
n_ell=100    		; number of log-spaced ell values to compute
ell_min=1e1   		; minimum ell value to compute
ell_max=3e4
verbosity=1
shear_shear=T
intrinsic_alignments=T
matter_spectra=T
ggl_spectra=T
gal_IA_cross_spectra=T
mag_gal_cross_spectra=T
mag_mag=T
mag_IA=T
mag_shear=T

; Short python module to display the resulting Cls
[plot_spectra]
file=modules/modules/bias_Joachimi_Bridle/plot_Cls_interface.py
survey=des
shear=T
intrinsic_alignments=T
clustering=T
magnification=T
